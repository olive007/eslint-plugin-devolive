/**
 * @fileoverview Remove JSX surrounding parentheses
 * @author Olivier
 */
"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/**
 * @type {import('eslint').Rule.RuleModule}
 */
module.exports = {
  meta: {
    type: "layout",
    docs: {
      description: "Remove JSX surrounding parentheses",
      category: "Fill me in",
      recommended: false,
      url: null, // URL to the documentation page for this rule
    },
    fixable: "code",
    schema: [], // Add a schema if the rule has options
  },

  create(context) {

    return {
      JSXElement: node => {

        const previousNode = context.getTokenBefore(node);
        const nextNode = context.getTokenAfter(node);

        if (previousNode && nextNode &&
          previousNode.type === "Punctuator" && previousNode.value === "(" &&
          nextNode.type === "Punctuator" && nextNode.value === ")") {

          const nodePreviousParent = context.getTokenBefore(previousNode);

          if (nodePreviousParent.type === 'Identifier') {
            return;
          }

          context.report({
            node,
            message: "Is those parentheses useful ?",
            loc: {
              start: previousNode.loc.start,
              end: nextNode.loc.end
            },
            * fix(fixer) {

              yield fixer.remove(previousNode);
              yield fixer.remove(nextNode);

              if (previousNode.loc.start.line !== node.loc.start.line) {

                yield fixer.removeRange([previousNode.range[1], node.range[0]]);
                yield fixer.removeRange([node.range[1], nextNode.range[0]]);
                yield fixer.insertTextBefore(node.parent, "");
                yield fixer.insertTextAfter(node.parent, "");
              }
            }
          })
        }
      }

    };
  },
};
