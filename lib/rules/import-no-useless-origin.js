/**
 * @fileoverview Remove useless origin folder into import statements
 * @author Olivier SECRET
 */
"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/**
 * @type {import('eslint').Rule.RuleModule}
 */
module.exports = {
  meta: {
    type: "suggestion",
    docs: {
      description: "Remove useless origin folder into import statements",
      category: "Import",
      recommended: true,
      url: null, // URL to the documentation page for this rule
    },
    fixable: "code", // Or `code` or `whitespace`
    schema: [], // Add a schema if the rule has options
  },

  create(context) {
    return {

      ImportDeclaration: node => {

        const importNames = node.specifiers
          .map(s => s.type === "ImportSpecifier" ? "{" + s.local.name + "}" : s.local.name)
          .join(", ");

        if (node.source.value.startsWith("./.") || node.source.value.includes("/./")) {

          context.report({
            node,
            message: "Import '{{ importNames }}' use useless parent origin folder",
            data: {
              importNames
            },

            fix(fixer) {

              const sourceFixed = node.source.value
                .split("/")
                .filter((s, index, tab) => s !== "." || (index < 1 && tab[index + 1] !== ".."))
                .join("/");

              return fixer.replaceTextRange([node.source.range[0] + 1, node.source.range[1] - 1], sourceFixed);
            }
          });
        }

      },
    };
  },
};
