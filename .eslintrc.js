"use strict";

module.exports = {

  root: true,

  extends: [
    "eslint:recommended",
    "plugin:eslint-plugin/recommended",
    "plugin:node/recommended",
  ],

  env: {
    node: true,
  },

  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    ecmaFeatures: { jsx: true }
  },

  overrides: [{

    files: ["tests/**/*.js"],

    env: { mocha: true }

  }],
};
