# Remove JSX surrounding parentheses (jsx-no-surrounding-parents)
Legacy code could sometime not readable due to prettier format.  
This rule fix it by removing the parentheses around JSX

## Rule Details

This rule aims to remove useless parentheses

Examples of **incorrect** code for this rule:

```js
return (<p>Hello</p>);
```

Examples of **correct** code for this rule:

```js
return <p>Hello</p>;
```

### Options

If there are any options, describe them here. Otherwise, delete this section.

## When Not To Use It

If you are using prettier this rule is not made for you.
