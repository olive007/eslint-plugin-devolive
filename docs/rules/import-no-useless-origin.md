# Remove useless origin folder into import statements (import-no-useless-origin)

Legacy code can sometimes have bad import path. This eslint rule fix them. 

## Rule Details

This rule aims to correct the import path. This avoids warning in WebStorm and fast-up the migration of legacy code 

Examples of **incorrect** code for this rule:

```js
import Foo from './.';
import Foo from './../foo';
import Foo, {Bar} from '.././foo';
```

Examples of **correct** code for this rule:

```js
import Foo from '.';
import Foo from './foo';
import Foo from '../foo';
```
