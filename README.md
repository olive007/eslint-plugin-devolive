# eslint-plugin-devolive

Multiple useful rules

## Installation

You will need to install [ESLint](https://eslint.org/) as well:

```sh
npm install --save-dev eslint eslint-plugin-devolive
```

## Usage

Add `devolive` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "devolive"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
      "devolive/import-no-useless-origin": "error",
      "devolive/jsx-no-surrounding-parents": "error"
    }
}
```

## Supported Rules

 * import-no-useless-origin
 * jsx-no-surrounding-parents
