const RuleTester = require("eslint").RuleTester;
const {parserOptions} = require("../.eslintrc.js");

RuleTester.setDefaultConfig({
  parserOptions
});

module.exports = RuleTester;
