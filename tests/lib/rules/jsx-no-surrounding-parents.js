/**
 * @fileoverview Remove JSX surrounding parentheses
 * @author Olivier
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/jsx-no-surrounding-parents");
const RuleTester = require("../../rules-tester");


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester();

ruleTester.run("my-id", rule, {
  valid: [
    "let toto = <p>Hello</p>;",
    "const toto = <p>Hello</p>;",
    "() => <p>Hello</p>",
    "(text) => <p>{text}</p>",
    "() => {return <p>Hello</p>}",
    "React.render(<p>Hello</p>);",
    "increase(<p>Hello</p>);",
  ],

  invalid: [{
    code: "let toto = (<p>Hello</p>);",
    output: "let toto = <p>Hello</p>;",
    errors: [{ message: "Is those parentheses useful ?", type: "JSXElement" }],
  }, {
    code: "let toto = x > 1 ? (<p>Hello</p>) : (<li>World</li>);",
    output: "let toto = x > 1 ? <p>Hello</p> : <li>World</li>;",
    errors: [
      { message: "Is those parentheses useful ?", type: "JSXElement" },
      { message: "Is those parentheses useful ?", type: "JSXElement" }
    ],
  }, {
    code: "let toto = (\n<div>\n<p>Hello</p>\n</div>\n);",
    output: "let toto = <div>\n<p>Hello</p>\n</div>;",
    errors: [{ message: "Is those parentheses useful ?", type: "JSXElement" }],
  }],
});
