/**
 * @fileoverview Remove useless origin folder into import statements
 * @author Olivier SECRET
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/import-no-useless-origin");
const RuleTester = require("../../rules-tester");

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester();
ruleTester.run("import-no-useless-origin", rule, {

  valid: [
    "import Foo from './foo';",
    'import Foo from "./foo";',
    'import Foo from "../../foo";',
    'import Foo, {Bar} from "../../foo";',
    'import Foo, {Bar} from ".";',
  ],

  invalid: [{
    code: "import Foo from '././.';",
    output: "import Foo from '.';",
    errors: [{
      message: "Import 'Foo' use useless parent origin folder",
      type: "ImportDeclaration"
    }],
  },{
    code: "import Foo from '././foo';",
    output: "import Foo from './foo';",
    errors: [{
      message: "Import 'Foo' use useless parent origin folder",
      type: "ImportDeclaration"
    }],
  }, {
    code: "import Foo from './././foo';",
    output: "import Foo from './foo';",
    errors: [{
      message: "Import 'Foo' use useless parent origin folder",
      type: "ImportDeclaration"
    }],
  }, {
    code: "import Foo, {Bar} from '././foo';",
    output: "import Foo, {Bar} from './foo';",
    errors: [{
      message: "Import 'Foo, {Bar}' use useless parent origin folder",
      type: "ImportDeclaration"
    }],
  }, {
    code: 'import Foo, {Bar} from "././foo";',
    output: 'import Foo, {Bar} from "./foo";',
    errors: [{
      message: "Import 'Foo, {Bar}' use useless parent origin folder",
      type: "ImportDeclaration"
    }],
  }, {
    code: "import Foo from './../foo';",
    output: "import Foo from '../foo';",
    errors: [{
      message: "Import 'Foo' use useless parent origin folder",
      type: "ImportDeclaration"
    }],
  }, {
    code: "import Foo from '.././foo';",
    output: "import Foo from '../foo';",
    errors: [{
      message: "Import 'Foo' use useless parent origin folder",
      type: "ImportDeclaration"
    }],
  }]
});
